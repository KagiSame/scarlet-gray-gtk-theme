***Scarlet-Gray-gtk-theme***

Scarlet Gray is a GTK+2 and GTK+3 theme inspired by popular app from Kinoshita Productions "Booru Nav"


**Pack contains 4 themes:**
Scarlet Gray Dark
Scarlet Gray Light
Scarlet Gray Halloween Dark
Scarlet Gray Halloween Light
